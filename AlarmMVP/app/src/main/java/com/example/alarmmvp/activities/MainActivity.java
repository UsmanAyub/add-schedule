package com.example.alarmmvp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alarmmvp.R;
import com.example.alarmmvp.activities.model.AlarmItem;
import com.example.alarmmvp.activities.model.MainModel;
import com.example.alarmmvp.activities.presenter.MainPresenter;
import com.example.alarmmvp.activities.view.AlarmAdapter;
import com.example.alarmmvp.activities.view.MainView;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView {

    ListView alarm_list;
    FloatingActionButton btn_add;
    List<AlarmItem> list;
    String title, hour, minute, days;

    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainModel(this);

        alarm_list = findViewById(R.id.alarm_list);
        alarm_list.setEmptyView(new TextView(this));
        btn_add = findViewById(R.id.btn_add);
        list = new ArrayList<>();

        title = getIntent().getStringExtra("title");
        hour = getIntent().getStringExtra("hour");
        minute = getIntent().getStringExtra("minute");
        days = "";

        boolean mo = getIntent().getBooleanExtra("mo", true);
        boolean tu = getIntent().getBooleanExtra("tu", true);
        boolean we = getIntent().getBooleanExtra("we", true);
        boolean th = getIntent().getBooleanExtra("th", true);
        boolean fr = getIntent().getBooleanExtra("fr", true);
        boolean sa = getIntent().getBooleanExtra("sa", true);
        boolean su = getIntent().getBooleanExtra("su", true);

        if (mo) days = days+"Mo, ";
        if (tu) days = days+"Tu, ";
        if (we) days = days+"We, ";
        if (th) days = days+"Th, ";
        if (fr) days = days+"Fr, ";
        if (sa) days = days+"Sa, ";
        if (su) days = days+"Su, ";


        if (title == null || hour == null)
        {

        }
        else
        {

            list.add(new AlarmItem(title, hour, minute, days));
            mainPresenter.populateList(title,hour,minute,days);
            alarm_list.setAdapter(new AlarmAdapter(getApplicationContext(), R.layout.alarm_item, list));
        }

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), AddActivity.class));

            }
        });


    }

    @Override
    public void listPopulated() {

        Toast.makeText(MainActivity.this, "Alarm Set Successfully !", Toast.LENGTH_SHORT).show();

    }
}
