package com.example.alarmmvp.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alarmmvp.R;
import com.example.alarmmvp.activities.model.AlarmActivator;
import com.example.alarmmvp.activities.view.TimePicker;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener{

    EditText edit_title;
    TextView edit_time;
    CheckBox mo,tu,we,th,fr,sa,su;
    Button btn_add_alarm;
    int mHour, mMinute;
    String title, hour, minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        edit_title = findViewById(R.id.edit_title);
        edit_time = findViewById(R.id.edit_time);
        mo = findViewById(R.id.monday);
        tu = findViewById(R.id.tuesday);
        we = findViewById(R.id.wednesday);
        th = findViewById(R.id.thursday);
        fr = findViewById(R.id.friday);
        sa = findViewById(R.id.saturday);
        su = findViewById(R.id.sunday);

        btn_add_alarm = findViewById(R.id.btn_add_alarm);


        DialogFragment timePicker = new TimePicker();
        timePicker.show(getSupportFragmentManager(), "Time Picker");

        btn_add_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                title = edit_title.getText().toString();
                if (mHour<10) {hour = "0"+mHour;}
                else {hour = ""+mHour;}
                if (mMinute<10) {minute= "0"+mMinute;}
                else {minute = ""+mMinute;}


                Intent intent = new Intent(AddActivity.this, MainActivity.class);
                intent.putExtra("title",title);
                intent.putExtra("hour", hour);
                intent.putExtra("minute", minute);
                intent.putExtra("mo", mo.isChecked());
                intent.putExtra("tu", tu.isChecked());
                intent.putExtra("we", we.isChecked());
                intent.putExtra("th", th.isChecked());
                intent.putExtra("fr", fr.isChecked());
                intent.putExtra("sa", sa.isChecked());
                intent.putExtra("su", su.isChecked());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        mHour = hourOfDay;
        mMinute = minute;
        edit_time.setText(""+mHour+":"+mMinute);

        if (mHour < 10 && mMinute < 10)
        {
            edit_time.setText("0"+mHour+":0"+mMinute);
        }
        else
        {
            if (mHour < 10 && mMinute >=10)
            {
                edit_time.setText("0"+mHour+":"+mMinute);
            }
            else
            {
                if (mHour > 10 && mMinute <10)
                {
                    edit_time.setText(""+mHour+":0"+mMinute);
                }
                else
                {
                    edit_time.setText(""+mHour+":"+mMinute);
                }

            }
        }

        Toast.makeText(getApplicationContext(), ""+hourOfDay+minute, Toast.LENGTH_SHORT).show();
        startAlarm();
    }

    public void startAlarm()
    {
        Intent intent = new Intent(getApplicationContext(), AlarmActivator.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 23433, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar cal_alarm = Calendar.getInstance();
        cal_alarm.set(Calendar.HOUR_OF_DAY, mHour);
        cal_alarm.set(Calendar.MINUTE, mMinute);
        cal_alarm.set(Calendar.SECOND, 0);

        alarmManager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis(), pendingIntent);

    }
}
